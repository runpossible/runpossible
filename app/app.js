define([
	'angular',
	'ui.router',
	'./js/home/home',
    './js/activities/activities'
], function(angular, require) {
    var runApp = angular.module('runApp', [
    	'ui.router', 
    	'home',
        'activities'
    ]);

/*
    runApp.config(function($stateProvider) {
        $stateProvider.state('base', {
                url: '',
                template: '<ui-view>hello</ui-view>'
            });
    });*/

    return runApp;
});
