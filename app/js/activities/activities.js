define(function(require) {
	var angular = require('angular');
	
    var activities = angular.module('activities', ['ui.router']);
    var activitiesListCtrl = require('/js/activities/activities-list.js');
    
    activities.controller('ActivitiesListCtrl', activitiesListCtrl);

	activities.config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('base.activities', {
                url: '/activities',
                templateUrl: '/js/activities/activities-list.html',
                controller: 'ActivitiesListCtrl'
            });
    }]);

    return activities;
});
