define([], function () {
    'use strict';

    return ['$scope', function($scope) {
        $scope.activitiesCount = 42;
    }];
});
