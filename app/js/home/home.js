define(function(require) {
	var angular = require('angular');
	
	var home = angular.module('home', ['ui.router']);
    var identity = require('/js/authentication/identity.js');
    var accountDir = require('/js/authentication/account.js');
    
    home.factory('Identity', identity);
    home.directive('account', accountDir);

	home.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/otherwise');

        $stateProvider.state('base', {
                url: '',
                templateUrl: './js/home/home.html'
            })
            .state("otherwise", { 
                url : '/otherwise',
                templateUrl: './js/home/home.html'
            });
    }]);

    return home;
});
