define([], function () {
    'use strict';

    return ['$http', 'Identity', '$location', function($http, Identity, $location) {
        return {
            restrict: 'E',
            templateUrl: 'js/authentication/account.html',
            link: function ($scope) {
                $scope.Identity = Identity;
        
                $scope.site_url = encodeURIComponent(window.location.protocol + '//' + window.location.host );
        
                $scope.signin = function() {
                    $http.post('v1/api/login', { username: $scope.username, password: $scope.pass })
                        .then(function(response) {
                            if(response.data.success) {
                                console.log('logged in!');
                                Identity.currentUser(response.data.user);
                            } else {
                                console.log('failed to log in!');
                                Identity.currentUser('');
                            }
                        });
                };

                $scope.signout = function() {
                    $http.post('v1/api/logout', { logout: true }).then(function() {
                        $scope.username = '';
                        $scope.pass = '';

                        Identity.currentUser('');

                        $location.path('/');
                    });
                };
            }
        };
    }];
});
