define(function(require) {
	'use strict';

	return ['$window', function($window) {
		var _currentUser = $window.currentUser;

		return {
			currentUser: function(user) {
				// Get user
				if(arguments.length === 0) {
					if(!_currentUser || _currentUser === '') return undefined;

					return _currentUser;
				}

				// Set user
				if(!user) {
					_currentUser = undefined;
					return;
				}
				
				_currentUser = user;
			},

			isAuthenticated: function() {
				return !!this.currentUser();
			}
		}
	}];

});