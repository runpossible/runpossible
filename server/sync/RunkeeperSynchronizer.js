var runkeeper = require('runkeeper-js'),
    _ = require('lodash'),
    activityInserter = require('./ActivityInserter')();

var PROVIDER_NAME = 'runkeeper';

exports = module.exports = function () {
    var getActivities = function getActivities(db, email, client, endpoint, getActivitiesDone) {
        client.apiCall("GET", "application/vnd.com.runkeeper.FitnessActivityFeed+json", endpoint, function (err, reply) {
            if (err) {
                // maybe the access token has expired.
                throw new Error(err);
            }

            activityInserter.saveActivities(syncProvider(), db, email, reply.items, function (err, hasMore) {
                if (err) {
                    throw new Error(err);
                }

                // if all activities were new to us and there are more on the server
                // go fetch and sync.
                if (hasMore && reply.next) {
                    process.nextTick(function () {
                        getActivities(db, email, client, reply.next, getActivitiesDone);
                    });
                }
                else
                {
                    getActivitiesDone();
                }
            });
        });
    };

    var saveRunkeeperProfile = function saveRunkeeperProfile(client, user) {
        client.profile(function (err, reply) {
            if (err) {
                console.log(err);
            }

            user.runkeeper.profile = reply.profile;
            user.save(function (err) {
                if (err) {
                    console.log('User save failed: ' + err);
                }
                console.log('User runkeeper profile saved');
            });
        });
    };

    var canSync = function canSync(user) {
        return user.runkeeper && user.runkeeper.profileId;
    };
    
    var sync = function sync(config, db, user, done) {
        var opts = {
            client_id: config.auth.runkeeper.client_id,
            client_secret: config.auth.runkeeper.client_secret,
            access_token: user.runkeeper.token // use the user access_token
        };

        var client = new runkeeper.HealthGraph(opts);

        console.log("Synchronizing Runkeeper account of user with email: " + user.email);

        // Save the Runkeeper Profile if we don't have it.
        if (!user.runkeeper.profile) {
            saveRunkeeperProfile(client, user);
        }

        getActivities(db, user.email, client, "/fitnessActivities", done);
    };

    var syncProvider = function(){
        var filter = function(userEmail){
            return {
                $and: [{
                    user_email: userEmail
                }, {
                    runkeeper: {
                        $exists: true
                    }
                }]
            };
        };

        var checkIfActivityExists = function(userActivity, activity){
            return userActivity.runkeeper.uri === activity.uri;
        };

        return {
            filter: filter,
            checkIfActivityExists: checkIfActivityExists,
            name: PROVIDER_NAME
        }
    };

    return {
        canSync: canSync,
        sync: sync
    }
};