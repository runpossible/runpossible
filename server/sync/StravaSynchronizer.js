var _ = require('lodash'),
    async = require('async'),
    activityInserter = require('./ActivityInserter')();

var PROVIDER_NAME = 'strava';

exports = module.exports = function () {
    var page_size = 50;

    var saveStravaProfile = function (strava, user) {
        strava.athlete.get(function (err, reply) {
            if (err) {
                console.log(err);
            }

            // TODO: improve this
            user.strava.profile = reply;
            user.save(function (err) {
                if (err) {
                    console.log('User save failed: ' + err);
                }
                console.log('User strava profile saved');
            });
        });
    };

    var getActivities = function getActivities(db, email, strava, after, getActivitiesDone) {
        strava.athlete.activities.get({ after: after}, function(err, res) {
            if (err) {
                // maybe the access token has expired.
                throw new Error(err);
            }

            activityInserter.saveActivities(syncProvider(), db, email, res, function (err, hasMore) {
                if (err) {
                    throw new Error(err);
                }

                // if all activities were new to us and there are more on the server
                // go fetch and sync.
                if (hasMore && res.length > 0) {
                    process.nextTick(function () {
                        getAfterValue(db, email, function(calculatedAfter){
                            getActivities(db, email, strava, calculatedAfter, getActivitiesDone);
                        })
                    });
                }
                else
                {
                    getActivitiesDone();
                }
            });
        });
    };

    var canSync = function canSync(user) {
        return user.strava && user.strava.profileId;
    };

    var sync = function sync(config, db, user, done) {

        var strava = require("strava")({
            client_id: config.auth.strava.client_id,
            client_secret: config.auth.strava.client_secret,
            redirect_uri: config.auth.strava.callbackURL,
            access_token: user.strava.token
        });

        console.log("Synchronizing Strava account of user with email: " + user.email);

        getAfterValue(db, user.email, function(calculatedAfter){
            // Save the Strava Profile if we don't have it.
            /*if (!user.strava.profile) {
             saveStravaProfile(strava, user);
             }*/

            getActivities(db, user.email, strava, calculatedAfter, done);
        })
    };

    var getAfterValue = function (db, email, found){
        var lastActivity = db.Activity.findOne(syncProvider().filter(email)).sort('-strava.start_date').limit(1);

        lastActivity.exec(function(err, item){
            if (err) {
                // maybe the access token has expired.
                throw new Error(err);
            }

            var after = 0;

            if(item)
            {
                after = item.strava.start_date.getTime() / 1000;
            }

            found(after);
        });
    };

    var syncProvider = function(){
        var filter = function(userEmail){
            return {
                $and: [{
                    user_email: userEmail
                }, {
                    strava: {
                        $exists: true
                    }
                }]
            };
        };

        var checkIfActivityExists = function(userActivity, activity){
            return userActivity.strava.id === activity.id;
        };

        return {
            filter: filter,
            checkIfActivityExists: checkIfActivityExists,
            name: PROVIDER_NAME
        }
    };

    return {
        canSync: canSync,
        sync: sync
    }
};