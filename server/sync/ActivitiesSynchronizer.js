var _ = require('lodash'),
    async = require('async');

var providersMap = {
    runkeeperSynchronizer: require('./RunkeeperSynchronizer')(),
    stravaSynchronizer: require('./StravaSynchronizer')()
};

var providersArray = _.toArray(providersMap);

exports = module.exports = {
    initialize: function (config, db) {
        this.config = config;
        this.db = db;
    },

    sync: function (user, done) {
        var self = this;

        async.each(providersArray,
            function (provider, callback) {
                if (provider.canSync(user)) {
                    return provider.sync(self.config, self.db, user, callback);
                }

                // no need to sync, continue.
                callback();
            },
            done);
    }
};