var async = require('async'),
    _ = require('lodash');

exports = module.exports = function () {
    var saveActivities = function(syncProvider, db, userEmail, activities, done) {
        db.Activity.find(syncProvider.filter(userEmail)).exec(function (err, dbActivities) {
            var userActivities = dbActivities;

            // filter out only the activities which we don't have
            var newActivities = _.filter(activities, function (activity) {
                var alreadySynced = _.any(userActivities, function (userActivity) {
					return syncProvider.checkIfActivityExists(userActivity, activity);
                    //return userActivity.runkeeper && userActivity.runkeeper.uri === activity.uri;
                });

                return !alreadySynced;
            });


            // sync each activity
            async.each(newActivities,
                function syncSingleActivity(newActivity, callback) {
                    var activity = new db.Activity();

                    activity.user_email = userEmail;

                    //
                    activity[syncProvider.name] = _.clone(newActivity);

                    // save our user to the database
                    activity.save(function (err) {
                        if (err) {
                            console.log(err);
                            return callback(err);
                        }

                        callback();
                    })
                },
                function doneEach(err) {
                    var hasMoreUnsyncedActivities = newActivities.length === activities.length;
                    done(err, hasMoreUnsyncedActivities);
                });
        });
    };
    
    return {
        saveActivities: saveActivities
    }
}