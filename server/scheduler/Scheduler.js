var async = require('async');

var Scheduler = function(config) {
    this.config = config || {
        scheduler: {
            enabled: true
        }
    };
};

/**
 * Run the scheduler - an infinite loop, which gets ready to process tasks and executes them.
 */
Scheduler.prototype.run = function() {
    var self = this;

    async.whilst(
        // test function
        function() {
            return self.config.scheduler.enabled;
        },
        function (callback) {
            // do work
            /* Works - just stopped for now.
            setTimeout(function() {
                //console.log('in main while cycle. at: ' + new Date());

                callback();
            }, 1000);
            */
        }, function (err) {
            console.log('scheduler run failed: ' + err);
            // TODO: improve exception logging.
        }
    )
};

exports = module.exports = Scheduler;