var express = require('express'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    passport = require('passport'),
    fs = require('fs'),
    cheerio = require('cheerio'),
    common = require('./common'),
    MongoStore = require('connect-mongo')(session),
    ActivitiesSynchronizer = require('../sync/ActivitiesSynchronizer'),
    _ = require('lodash'),
    async = require('async');

var executionInProgress = false;


module.exports = function (app, config, db) {
    ActivitiesSynchronizer.initialize(config, db);

    app.set('port', config.port);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(cookieParser());

    // Set MongoDB persistent session - user receives a cookie named connect.sid
    // Authentication is based on this session.
    app.use(session({
        secret: 'runpossible secret.',
        resave: true,
        saveUninitialized: true,
        cookie: {
            maxAge: 2592000000 // one month
        },
        store: new MongoStore({mongooseConnection: db.Mongoose.connection})
    }));

    app.use(passport.initialize());
    app.use(passport.session());

    // When getting the /index.html, inject the user in the window.currentUser variable.
    app.get('/', function (req, res) {
        var html = fs.readFileSync(config.rootPath + '/app/index.html', 'utf8');
        if (!req.user) {
            return res.send(html);
        }

        var $ = cheerio.load(html),
            userString = JSON.stringify(common.sanitizeUser(req.user));
        var userScript = '<script>window.currentUser = JSON.parse(\'' + userString + '\')</script>';
        $('body').prepend(userScript);

        res.send($.html());
    });

    app.get('/execute', function (req, res) {
        if (executionInProgress) {
            res.status(400).send('Execution in progress').end();
            return;
        }
        else {
            executionInProgress = true;
            // Get all users
            db.User.find({}).exec(function (err, users) {
                async.each(users,
                    function (user, callback) {
                        console.log('Synchronizing user: ' + user.email);
                        ActivitiesSynchronizer.sync(user, function (err) {
                            if (err) {
                                console.log('Error: Sync not finished successfully for user ' + user.email + ' with error: ' + err);
                            }

                            callback();
                        });
                    }, function done() {
                        executionInProgress = false;
                    });
            });
            res.status(200).send('Execution started').end();
        }
    });

    app.post('/register', function(req, res, next) {
        var email = req.body.email,
            pwd = req.body.password;

        if(!email || !pwd) {
            return next(new Error('The email and password are required for new registrations.'));
        }

        var salt = db.User.createSalt(),
            hash = db.User.hashPwd(salt, pwd);
        var user = {
            email: email,
            local: {
                salt: salt,
                hashed_pwd: hash
            }
        };

        db.User.create(user, function(err, createdUser) {
            if(err) return next(err);

            res.status(201).send({ success: true });
        });
    });

    app.use(express.static(config.rootPath + '/app'));

    app.use(function onError(err, req, res, next) {
        console.error(err.stack);
        res.send(500, {
            message: err.message
        });
    });

}