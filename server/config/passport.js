var passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy,
	FacebookStrategy = require('passport-facebook').Strategy,
	GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
	RunKeeperStrategy = require('passport-runkeeper').Strategy,
	StravaStrategy = require('passport-strava').Strategy,
	common = require('./common');


exports = module.exports = function(_db, _config) {
	var db = _db;
	var config = _config;

	// Local Strategy
	passport.use(new LocalStrategy(
		function(email, password, done) {
			//debugger;
			db.User.findOne({
				"email": email
			}).exec(function(err, user) {
				if (err) {
					return done(err);
				}

				if (user && user.authenticateLocal(password)) {
					return done(null, common.sanitizeUser(user));
				}
				return done(null, false);
			});
		}
	));

	passport.serializeUser(function(user, done) {
		done(null, user._id);
	});

	passport.deserializeUser(function(id, done) {
		db.User.findOne({
			_id: id
		}).exec(function(err, user) {
			if (err) return done(err);

			if (user) return done(null, user);
			return done(null, false);
		});
	});

	// TODO: Refactor
	var getUserByEmail = function(email, done) {
		db.User.findOne({
			'email': email
		}, function(err, user) {
			if (err) return done(err);
			return done(null, user);
		});
	};

	// Facebook - https://scotch.io/tutorials/easy-node-authentication-facebook
	passport.use(new FacebookStrategy({
			clientID: config.auth.facebook.client_id,
			clientSecret: config.auth.facebook.client_secret,
			callbackURL: config.auth.facebook.callbackURL,
			profileFields: ['id', 'displayName', 'link', 'photos', 'emails']
		},
		function(accessToken, refreshToken, profile, done) {
			// asynchronous
			process.nextTick(function() {

				// find the user in the database based on their facebook id
				db.User.findOne({
					'facebook.id': profile.id
				}, function(err, user) {

					// if there is an error, stop everything and return that
					// ie an error connecting to the database
					if (err)
						return done(err);

					// if the user is found, then log them in
					if (user) {
						return done(null, user); // user found, return that user
					} else {
						getUserByEmail(profile.emails[0].value, function(err, user) {
							if (err) return done(err);

							if (!user) {
								user = new db.User();
								// user doesn't exist, so set the primary e-mail
								user.email = profile.emails[0].value;
							}

							// set all of the facebook information in our user model
							user.facebook.id = profile.id; // set the users facebook id                   
							user.facebook.token = accessToken; // we will save the token that facebook provides to the user                    
							user.facebook.name = profile.displayName;
							//user.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first

							// save our user to the database
							user.save(function(err) {
								if (err)
									throw err;

								// if successful, return the new user
								return done(null, user);
							});
						});
					}

				});
			});
			//console.log('successfully logged in with FB! Profile = ' + JSON.stringify(profile));
		}
	));

	// =========================================================================
	// GOOGLE ==================================================================
	// =========================================================================
	passport.use(new GoogleStrategy({
			clientID: config.auth.google.client_id,
			clientSecret: config.auth.google.client_secret,
			callbackURL: config.auth.google.callbackURL,

		},
		function(token, refreshToken, profile, done) {
			// make the code asynchronous
			// User.findOne won't fire until we have all our data back from Google
			process.nextTick(function() {

				// try to find the user based on their google id
				db.User.findOne({
					'google.id': profile.id
				}, function(err, user) {
					if (err)
						return done(err);

					if (user) {
						// if a user is found, log them in
						return done(null, user);
					} else {
						getUserByEmail(profile.emails[0].value, function(err, user) {
							if (err) return done(err);

							if (!user) {
								user = new db.User();
								user.email = profile.emails[0].value;
							}

							// set all of the relevant information
							user.google.id = profile.id;
							user.google.token = token;
							user.google.name = profile.displayName;

							// save the user
							user.save(function(err) {
								if (err)
									throw err;
								return done(null, user);
							});
						});
					}
				});
			});
		}
	));

	// =========================================================================
	// JOBS ====================================================================
	// =========================================================================

	var ensureJob = function(type, data) {
		if(type === 'runkeeper') {
			// data.profileId && data.token
			db.Job.findOne({
				'runkeeper.profileId': data.profileId
			}, function(err, job) {
				if(err) {
					// todo: handle these errors
					console.log('couldn\'t create runkeeper job!!!');
					return;
				}

				if(job) {
					job.runkeeper.token = data.token;
					job.runkeeper.enabled = true;
					job.save(); // TODO: handle errors
				} else {
					job = new db.Job();
					job.runkeeper = data;
					job.runkeeper.enabled = true;
					job.save(); // TODO: handle errors
				}
			});
		}
		else if(type === 'strava'){
			// data.profileId && data.token
			db.Job.findOne({
				'strava.profileId': data.profileId
			}, function(err, job) {
				if(err) {
					// todo: handle these errors
					console.log('couldn\'t create strava job!!!');
					return;
				}

				if(job) {
					job.strava.token = data.token;
					job.strava.enabled = true;
					job.save(); // TODO: handle errors
				} else {
					job = new db.Job();
					job.strava = data;
					job.strava.enabled = true;
					job.save(); // TODO: handle errors
				}
			});
		}
	}

	// =========================================================================
	// RUNKEEPER ==================================================================
	// =========================================================================
	passport.use(new RunKeeperStrategy({
			clientID: config.auth.runkeeper.client_id,
			clientSecret: config.auth.runkeeper.client_secret,
			callbackURL: config.auth.runkeeper.callbackURL,
			passReqToCallback: true
		},
		function(req, accessToken, refreshToken, profile, done) {
			// make the code asynchronous
			// User.findOne won't fire until we have all our data back from Google
			process.nextTick(function() {

//				console.log(JSON.stringify(profile));
				// try to find the user based on their google id
				db.User.findOne({
					'runkeeper.profileId': profile.id
				}, function(err, user) {
					//debugger;
					if (err)
						return done(err);

					if (user) {
						//TODO: update token/refresh token here??? for FB & Google too
						ensureJob('runkeeper', user.runkeeper);
						user.runkeeper.token = accessToken;
						// save the user
						user.save(function(err) {
							if (err) throw err;

							// log the user in.
							return done(null, user);
						});
					} else {
						var email = req.user.email;
						
						getUserByEmail(email, function(err, user) {
							if (err) return done(err);

							if (!user) {
								return done('User is not authorized.');
							}

							// set all of the relevant information
							user.runkeeper = {
								profileId: profile.id,
								token: accessToken
							};

							ensureJob('runkeeper', user.runkeeper);

							// save the user
							user.save(function(err) {
								if (err)
									throw err;
								return done(null, user);
							});
						});
					}
				});
			});
		}
	));
	
	// =========================================================================
	// STRAVA ==================================================================
	// =========================================================================
	passport.use(new StravaStrategy({
			clientID: config.auth.strava.client_id,
			clientSecret: config.auth.strava.client_secret,
			callbackURL: config.auth.strava.callbackURL,
			passReqToCallback: true
		},
		function(req, accessToken, refreshToken, profile, done) {
			// make the code asynchronous
			// User.findOne won't fire until we have all our data back from Google
			process.nextTick(function() {

//				console.log(JSON.stringify(profile));
				// try to find the user based on their google id
				db.User.findOne({
					'strava.profileId': profile.id
				}, function(err, user) {
					//debugger;
					if (err)
						return done(err);

					if (user) {
						//TODO: update token/refresh token here??? for FB & Google too
						ensureJob('strava', user.strava);
						user.strava.token = accessToken;
						// save the user
						user.save(function(err) {
							if (err) throw err;

							// log the user in.
							return done(null, user);
						});
					} else {
						var email = req.user.email;
						
						getUserByEmail(email, function(err, user) {
							if (err) return done(err);

							if (!user) {
								return done('User is not authorized.');
							}

							// set all of the relevant information
							user.strava = {
								profileId: profile.id,
								token: accessToken
							};

							ensureJob('strava', user.strava);

							// save the user
							user.save(function(err) {
								if (err)
									throw err;
								return done(null, user);
							});
						});
					}
				});
			});
		}
	));
	
};