exports = module.exports = {
	sanitizeUser: function(user) {
	    var result = {
			_id: user._id,
			email: user.email,
			facebook: user.facebook,
			google: user.google,
			runkeeper: user.runkeeper,
			strava: user.strava
		};

		return result;
	}
};