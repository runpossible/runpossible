var path = require('path');
var rootPath = path.normalize(__dirname + '../../../');
exports = module.exports = {
	development: {
		port: process.env.PORT || 3000,
        ip_address: process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',
		site_address: "http://localhost:3000",
		rootPath: rootPath,
		dbConnString: 'mongodb://localhost/runpossible',
		auth: {
			facebook: {
				name: 'Facebook',
				client_id: 882557195159647,
				client_secret: 'e1a6f3e91561a80546949108df814ec7',
				callbackURL: "http://localhost:3000/v1/api/auth/facebook/callback",
			},
			google: {
				name: 'Google',
				client_id: '150797401432-o8fe23f4ak1o3oq56eev3idjrug59nnm.apps.googleusercontent.com',
				client_secret: 'HeNfX9qM-VMOFeSk0S1lcHXT',
				callbackURL: 'http://localhost:3000/v1/api/auth/google/callback'
			},
			runkeeper: {
				// Client ID (Required): 
			    // This value is the OAuth 2.0 client ID for your application.  
			    client_id : "eeb040061cfb486d98a4a90c5a984004",

			    // Client Secret (Required):  
			    // This value is the OAuth 2.0 shared secret for your application.   
			    client_secret : "7bfb0fed388c49669232f7b4098a5b70",

			    callbackURL: 'http://localhost:3000/v1/api/auth/runkeeper/callback'
			},
			strava:{
				// Client ID (Required): 
			    // This value is the OAuth 2.0 client ID for your application.  
			    client_id : "7562",

			    // Client Secret (Required):  
			    // This value is the OAuth 2.0 shared secret for your application.   
			    client_secret : "062bb084ea99495fa11a29cf83ba9b6a8290ede5",
				
				access_token: "e1b5fa6140b9b47a7157569b93c87d12b733185c",

			    callbackURL: 'http://site-runpossible.rhcloud.com/v1/api/auth/strava/callback'
			}
		}
	},
	production: {
        port: process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 3000,
        ip_address: process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',
		site_address: "http://site-runpossible.rhcloud.com/",
		rootPath: rootPath,
		dbConnString: 'mongodb://admin:pN8mKxQ2QFfC@' + process.env.OPENSHIFT_MONGODB_DB_HOST + ':' + process.env.OPENSHIFT_MONGODB_DB_PORT + '/', // openshift mongo
		auth: {
			facebook: {
				name: 'Facebook',
				client_id: 877406112341422,
				client_secret: '683be750c1855beff585d4a326ee4c1c',
				callbackURL: "http://site-runpossible.rhcloud.com/v1/api/auth/facebook/callback",
			},
			google: {
				name: 'Google',
				client_id: '150797401432-o8fe23f4ak1o3oq56eev3idjrug59nnm.apps.googleusercontent.com',
				client_secret: 'HeNfX9qM-VMOFeSk0S1lcHXT',
				callbackURL: 'http://site-runpossible.rhcloud.com/v1/api/auth/google/callback'
			},
			runkeeper: {
				// Client ID (Required): 
			    // This value is the OAuth 2.0 client ID for your application.  
			    client_id : "eeb040061cfb486d98a4a90c5a984004",

			    // Client Secret (Required):  
			    // This value is the OAuth 2.0 shared secret for your application.   
			    client_secret : "7bfb0fed388c49669232f7b4098a5b70",

			    callbackURL: 'http://site-runpossible.rhcloud.com/v1/api/auth/runkeeper/callback'
			},
			strava: {
				// Client ID (Required): 
			    // This value is the OAuth 2.0 client ID for your application.  
			    client_id : "7562",

			    // Client Secret (Required):  
			    // This value is the OAuth 2.0 shared secret for your application.   
			    client_secret : "062bb084ea99495fa11a29cf83ba9b6a8290ede5",
				
				access_token: "e1b5fa6140b9b47a7157569b93c87d12b733185c",

			    callbackURL: 'http://site-runpossible.rhcloud.com/v1/api/auth/strava/callback'
			}
		}
	}
};