var passport = require('passport');
var base = '/v1/api';
module.exports = function(app) {
	app.post(base + '/login', function(req, res, next) {
		var auth = passport.authenticate('local', function(err, user) {
			if (err) return next(err);

			if (!user) return res.send({
				success: false
			});

			req.logIn(user, function(err) {
				if (err) return next(err);

				res.send({
					success: true,
					user: user
				});
			});
		});
		auth(req, res, next);
	});

	app.post(base + '/logout', function(req, res, next) {
		req.logout();

		res.end();
	});

	// Redirect the user to Facebook for authentication.  When complete,
	// Facebook will redirect the user back to the application at
	//     /auth/facebook/callback
	app.get(base + '/auth/facebook', passport.authenticate('facebook', {
		scope: 'email'
	}));

	// Facebook will redirect the user to this URL after approval.  Finish the
	// authentication process by attempting to obtain an access token.  If
	// access was granted, the user will be logged in.  Otherwise,
	// authentication has failed.
	app.get(base + '/auth/facebook/callback',
		passport.authenticate('facebook', {
			successRedirect: '/',
			failureRedirect: '/login'
		}));


	app.get(base + '/auth/google', passport.authenticate('google', {
		scope: ['profile', 'email']
	}));

	// the callback after google has authenticated the user
	app.get(base + '/auth/google/callback',
		passport.authenticate('google', {
			successRedirect: '/',
			failureRedirect: '/login'
		}));

	app.get(base + '/auth/runkeeper', passport.authenticate('runkeeper'));

	app.get(base + '/auth/runkeeper/callback', passport.authenticate('runkeeper', {
		successRedirect: '/',
		failureRedirect: '/login'
	}));
	
	app.get(base + '/auth/strava', passport.authenticate('strava'));

	app.get(base + '/auth/strava/callback', passport.authenticate('strava', {
		successRedirect: '/',
		failureRedirect: '/login'
	}));

	// route middleware to make sure a user is logged in
	function isLoggedIn(req, res, next) {
		// if user is authenticated in the session, carry on
		if (req.isAuthenticated())
			return next();

		// if they aren't redirect them to the home page
		res.redirect('/');
	}
};