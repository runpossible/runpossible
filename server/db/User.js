var crypto = require('crypto');

exports = module.exports = function(mongoose) {
  var createSalt = function() {
    return crypto.randomBytes(128).toString('base64');
  };

  var hashPwd = function(salt, pwd) {
    var hmac = crypto.createHmac('sha1', salt);
    return hmac.update(pwd).digest('hex');
  };

  var userSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true
      },
    local: {
      salt: String,
      hashed_pwd: String
    },
    facebook: {
      id: String,
      token: String,
      email: String,
      name: String
    },
    google: {
      id: String,
      token: String,
      email: String,
      name: String
    },
    runkeeper: {
      profileId: Number,
      token: String,
      profile: String
    },
    strava: {
      profileId: Number,
      token: String
    }
  });

  userSchema.methods = {
    authenticateLocal: function(pwd) {
      return hashPwd(this.local.salt, pwd) === this.local.hashed_pwd;
    }
  };

  userSchema.statics.createSalt = createSalt;
  userSchema.statics.hashPwd = hashPwd;

  var User = mongoose.model('User', userSchema);

  return User;
};