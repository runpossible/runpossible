exports = module.exports = function(mongoose) {
  var runSchema = new mongoose.Schema({
	name: String,
	runAt: Date,
  });

  var Run = mongoose.model('Run', runSchema);

  // Automatically set the updated at
  runSchema.pre('save', function(next) {
    this.runAt = new Date();
    next();
  });

  return Run;
};