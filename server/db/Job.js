exports = module.exports = function(mongoose) {
  var jobSchema = new mongoose.Schema({
    runkeeper: {
      profileId: Number,
      token: String,
      enabled: Boolean
    },
    strava: {
      profileId: Number,
      token: String,
      enabled: Boolean
    },
    updatedAt: Date
  });

  var Job = mongoose.model('Job', jobSchema);

  // Automatically set the updated at
  jobSchema.pre('save', function(next) {
    this.updatedAt = new Date();
    next();
  });

  return Job;
};