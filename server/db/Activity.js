exports = module.exports = function(mongoose) {
  var SchemaTypes = mongoose.Schema.Types;
  
  var activitySchema = new mongoose.Schema({
    user_email: String,
    runkeeper: {
      duration: Number,
      entry_mode: String,
      has_path: Boolean,
      source: String,
      start_time: Date,
      tracking_mode: String,
      total_distance: SchemaTypes.Double,
      total_calories: Number,
      type: {type: String},
      uri: String
    },
    strava:{
      id: Number,
      external_id: String,
      resource_state: Number,
      name: String,
      distance: Number,
      moving_time: Number,
      elapsed_time: Number,
      total_elevation_gain: Number,
      type: {type: String},
      start_date: Date,
      start_date_local: Date,
      timezone: String,
      start_latlng: String,
      end_latlng: String,
      location_country: String,
      start_latitude: String,
      start_longitude: String,
      achievement_count: Number,
      kudos_count: Number,
      comment_count: Number,
      athlete_count: Number,
      photo_count: Number,
      trainer: Boolean,
      commute: Boolean,
      manual: Boolean,
      private: Boolean,
      flagged: Boolean,
      average_speed: Number,
      max_speed: Number,
      total_photo_count: Number,
      has_kudoed: Boolean,
      workout_type: String
    },
    updatedAt: Date
  });

  var Activity = mongoose.model('Activity', activitySchema);

  // Automatically set the updated at
  activitySchema.pre('save', function(next) {
    this.updatedAt = new Date();
    next();
  });

  return Activity;
};