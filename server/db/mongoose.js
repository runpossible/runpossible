var mongoose = require('mongoose');
require('mongoose-double')(mongoose);

exports = module.exports = function(config) {
	mongoose.connect(config.dbConnString);

	var db = mongoose.connection;
	
	db.on('error', function callback(err) {
		console.error.bind(console, 'connection error: ' + err);
	});

	db.once('open', function callback() {
	  console.log('Connected to DB');
	});

	var User = require('./User')(mongoose);
	var Job = require('./Job')(mongoose);
	var Run = require('./Run')(mongoose);
	var Activity = require('./Activity')(mongoose);

	return {
		User: User,
		Job: Job,
		Run: Run,
		Mongoose: mongoose,
		Activity: Activity
	};
};