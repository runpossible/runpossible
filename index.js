var express = require('express');
var app = express();

var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

if (process.env.OPENSHIFT_NODEJS_PORT) {
    env = process.env.NODE_ENV = 'production';
}

// load configuration
var config = require('./server/config/config')[env];

console.log(config.rootPath);

// connect to database
var db = require('./server/db/mongoose')(config);

// bootstrap express application
require('./server/config/express')(app, config, db);

// add authentication layer
require('./server/config/passport')(db, config);

// add routes
require('./server/config/routes')(app);

// run scheduler
var Scheduler = require('./server/scheduler/Scheduler');
var schedulerInstance = new Scheduler();
schedulerInstance.run();

// start server
app.listen(config.port, config.ip_address, function (err) {
    if (err) {
        console.log('Node server failed to start! Error = ' + err);
        return;
    }

    console.log("Node server running at http://localhost:" + config.port);
});
